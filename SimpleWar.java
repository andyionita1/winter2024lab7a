public class SimpleWar {
	
	public static void main (String[] args) {
		
		Deck deck = new Deck();
		int player1Points = 0;
		int player2Points = 0;
		
		int round = 0;
		
		Card card = new Card("Hearts", "6");
		
		deck.Shuffle();
		
		System.out.println(deck);
		
		System.out.println("---------------");
		
		
		
		
		while (deck.Length() > 0) {
			
			Card card1 = deck.drawTopCard();
			Card card2 = deck.drawTopCard();
			
			round++;
			
			System.out.println();
			System.out.println("ROUND " + round);
			System.out.println();
			System.out.println("Player 1: " + card1);
			System.out.println("Player 2: " + card2);
			
			
			System.out.println("BEFORE ROUND " + round);
			System.out.println("Player 1 points: " + player1Points);
			System.out.println("Player 2 points: " + player2Points);
			
			if (card1.calculateScore() > card2.calculateScore()) {
				
				System.out.println(card1.getSuit() + " is bigger than " + card2.getSuit() + ", so player 1 wins");
				player1Points += 1;		
				
			}		
			else { 
			System.out.println(card2.getSuit() + " is bigger than " + card1.getSuit() + ", so player 2 wins");
			player2Points += 1;
			}
			
			System.out.println("AFTER ROUND " + round);
			System.out.println("Player 1 points: " + player1Points);
			System.out.println("Player 2 points: " + player2Points);
			
		}
		
		if (player1Points > player2Points) {
			System.out.println("Congratulations player 1 for winning with " + player1Points);
		} else {
			System.out.println("Congratulations player 2 for winning with " + player2Points);
		}
	}
}