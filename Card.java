public class Card {

    private String suit;
    private String value;

    public Card (String suit, String value) {
        this.suit = suit;
        this.value = value;
    }

    //Get values for each field
    public String getSuit() {
        return this.suit;
    }

    public String getValue() {
        return this.value;
    }

    public String toString() {
        return this.value + " of " + this.suit;
    }
	
	public double calculateScore() {
		
		double score = 0;
		score = Integer.parseInt(this.value);		
		
		if (this.suit.equals("Hearts")) {
			score += 0.4;
		} else if (this.suit.equals("Diamonds")) {
			score += 0.3;
		} else if (this.suit.equals("Clubs")) {
			score += 0.2;
		} else if (this.suit.equals("Spades")) {
			score += 0.1;
		}
		
		return score;
	}
}