import java.util.Random;

public class Deck {
    
    private Card[] cards;
    private int numberOfCards;
    private Random randomizer;
    
    public Deck() {
        
        Random randomizer = new Random();
        
        this.randomizer = randomizer;
        this.cards = new Card[52];
        this.numberOfCards = 52;
        
        String[] suits = new String[] {"Hearts", "Diamonds", "Clubs", "Spades"};
        String[] values = new String[] {"1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "11", "12", "13"};
        
        int position = 0;
        
        for (int i = 0; i < suits.length; i++) {
            for (int j = 0; j < values.length; j++) {
                cards[position] = new Card(suits[i], values[j]);
                position++;
            }
        }
    }
    
    public int Length() {
        return this.numberOfCards;
    }
    
    public Card drawTopCard() {
        this.numberOfCards--;
        return this.cards[numberOfCards];
    }
    
    public String toString() {
        
        String listOfCards = ""; 
        
        for (int i = 0; i < this.cards.length; i++) {
            listOfCards +=  this.cards[i] + "\n";
        }
        
        return listOfCards;
    }
    
    public void Shuffle() {
        
        Card keeper = this.cards[0];
        
        for (int i = 0; i < this.cards.length-1; i++) {
            
            int randomNumber = this.randomizer.nextInt(this.cards.length);
            
            keeper = this.cards[i];
            cards[i] = cards[randomNumber];
            cards[randomNumber] = keeper;
        }
    }
}